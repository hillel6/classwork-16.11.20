from flask import Flask, render_template, request

# Инициализация приложения Flask
app = Flask(__name__)


@app.route('/')
def get_index():
    #
    # В Flask можно создать самую простую страничку просто вернув строку
    #
    return "<h1>Its index page</h1>"


@app.route('/catalog')
def get_catalog():
    #
    # Также фласк позволяет уже готовые html странички
    #
    return render_template('catalog.html')
    

@app.route('/row/<item>')
def get_row(item='Default value'):
    #         |
    #   Flask позволяет принимать различные аргументы
    #   из самого пути запроса. Как пример вот такой адрес
    #   >>> localhost:5000/item/apple
    #
    #   подойдет такой страничке
    #   >>> /item/<item>
    #
    #   И далее в функции мы можем забрать этот параметр из
    #   аргументов функции и передать его в темплейт
    #                                         |
    return render_template('item.html', item=item)
    
    
@app.route('/product')
def get_product_by_args():
    # item = request.args['item']
    #                 | 
    #     в args хранятся все параметры которые 
    #     мы передаем в адрессной строке после знака ?
    #     Пример:
    #     >>> localhost:5000/product?item=First
    #
    #     и далее если сделать принт request.args
    #     то Flask выдаст словарь из всех аргументов
    #     переданных после знака ?
    #     >>> {'item': First}
    #
    #     Поскльку это обычный словарь то мы можем работать
    #     с ним как с обычным словарем Питона
    #     Тоесть взять параметр через квадратные скобки
    #     >>> request.args['item']
    #
    #     Но в случае если этот параметр не передать в строке
    #     >>> localhost:5000/product
    #     То питон выдаст ошибку KeyError поскольку такого ключа в словаре нет
    #     чтобы обойти эту ошибку можно завернуть в try except или воспользоватся
    #     готовым методом в словаре - .get()
    #     >>> request.args.get('item', 'Default value if item doesnt exist')
    #
    item = request.args.get('item', 'Default value')

    return render_template('item.html', item=item)


"""
Лучше запуск сервера app.run(debug=True)
вставлять в условие if __name__ == '__main__':
это нужно для того чтобы этот файл не запустил сервер
если заимпортить этот файл из другого питоновского файла 
"""
if __name__ == '__main__':
    app.run(debug=True)
